try:
    from setuptools import setup
except ImportError:
    from distutils.core import setup

config = {
    'description': 'PRI EPITA',
    'author': 'Pratik BHOIR',
    'url': 'http://pratikbh.info/project/templates/index.html',
    'author_email': 'pratikabhoir@gmail.com',
    'version': '0.1',
    'install_requires': ['nose'],
    'packages': ['NAME'],
    'scripts': ['cgi'],
    'name': 'Automatized distributed working environment'
}

setup(**config)